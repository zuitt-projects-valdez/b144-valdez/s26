/* 
  use the require directive to load node.js modules
  modules - software component or part of a program that contains one or more routines 
  ex. http module 
    - lets node transfer data using hypertext transfer protocol; 
    - set of indiv files that contains code to create a component that helps establish data transfer between apps
    - http allows communication between clients (browser) and server (node/express apps) by exchanging indiv messages 
      - message sent by the client, usually a web browsers = requests 
        - http://home  - requests and responses pass through the http (url)
      - message sent by the server as an answer = responses 
*/
let http = require("http"); //obtains modules to use (software components/built in packages)

//createServer() method - used to create an http server that listens to requests; accepts a function as an argument on a specified port (address/number in the url)and gives responses back to the client
// (request, response) - parameters in the function for client server
http
  .createServer(function (request, response) {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Goodbye");
    //response - coming from the parameter
    // writeHead - similar to head tag, includes status code, and type of data; status shows us if the connection is working (ex. 404 - requested data not found); 200+ status code: successful; 400+ status code: client issue; 500+ status code: server error/issue
    // use the writehead method to set a status code for the response - 200 means ok/successful
    // content type - set the content-type of the response as a plain text message
    //.end() - method that ends the response process; where the response ends;
  })
  .listen(4000); //port 4000 will receive and respond; a port is a virtual point where network connections start and end; each port is associated with a specific process or service; servers are assigned to port
//listen method - to indicate which port will be assigned to the server; server will listent o any requests sent to our server
//port 4000 is default for backend - but can be anything really as long as its not currently running in the current device

//when a server is running, console will print this message - lets us know the server is working
// node fileName.js
console.log("Server running at localhost:4000");
//http://localhost:4000 - url that will clients and servers will communicate through
