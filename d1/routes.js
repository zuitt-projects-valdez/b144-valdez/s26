const http = require("http");

//create variable port to store port number
const port = 4000;

//create a variable server that stores the output of the create server method
const server = http.createServer((req, res) => {
  //http://localhost:4000/greeting
  if (req.url == "/greeting") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Hello Again ");
  }

  //access the /homepage routes returns a message of "This is the homepage" with a 200 status
  else if (req.url == "/homepage") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("This is the homepage");
  } else {
    //all other routes return a message not found with a status of 404
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("Page not found");
  }
});

//use the server and port variables created above
server.listen(port);

//when server is running, console will print the message:
console.log(`Server now accessible at localhost:${port}`);

//nodemon is a package that automatically restarts the server connection; nodemon fileName.js

//http://localhost:4000/home/about/contacts - routes
//.url - link to be put in the browser
